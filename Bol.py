import base64
import datetime
import hashlib
import hmac
import requests


def _generate_bol_date() -> str:
    now = datetime.datetime.utcnow().strftime("%a, %d %b %Y %H:%M:%S GMT")
    return now


def _build_signature(now: str) -> str:
    http_verb = 'GET'
    content_type = 'application/xml; charset=UTF-8'
    x_bol_date = now
    uri = '/services/rest/orders/v2'
    signatureString = http_verb + '\n\n' + content_type + '\n' + x_bol_date + '\n' + 'x-bol-date:' + x_bol_date + '\n' + uri
    return signatureString


def _crypt_signature(signature: str, privatekey: str) -> str:
    b256 = hmac.new(privatekey.encode(), msg=signature.encode(), digestmod=hashlib.sha256).digest()
    b64 = base64.b64encode(b256)
    return b64.decode()


def _build_header(signature: str, publicKey: str, now: str) -> dict:
    d = dict()
    d["X-BOL-Authorization"] = publicKey + ":" + signature
    d["X-BOL-Date"] = now
    d["Accept"] = "application/xml; charset=UTF-8"
    d["Content-Type"] = "application/xml; charset=UTF-8"
    return d


def get_auth_header(privateKey: str, publicKey: str) -> dict:
    now = _generate_bol_date()
    signature = _crypt_signature(_build_signature(now), privateKey)
    return _build_header(signature, publicKey, now)


def get_orders(url: str, header: dict) -> str:
    r = requests.get(url, headers=header)
    return r.text
